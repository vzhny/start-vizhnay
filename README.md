A simple React powered start page, utilizing local storage to save and display the user's saved links.

Live link at https://start.vizhnay.io
